var Physics = require('io.platino.physicsjs'),
    platino = require('io.platino'),
    _ = require('alloy/underscore');

var controller = {
    args : arguments[0] || {},
    vars : {
        game : false,
        win : false,
        world : false,
        halfScreen : false
    },
    init : function() {
        controller.addHandlers();
        controller.resume();
    },
    physics : {
        init : function(world) {
            controller.vars.world = world;
            controller.physics.addBehaviors();
            controller.physics.addBodies();

        },
        addBehaviors : function() {

            Physics.behavior('runner-constraints', function(parent) {
                var defaults = {
                    vel : {
                        x : 0,
                        y : 0
                    }
                };
                return {
                    init : function(opts) {
                        parent.init.call(this, opts);
                        this.options.defaults(defaults);
                        this.options(opts);

                        this._vel = Physics.vector();
                        this.setVelocity(this.options.vel);
                        delete this.options.vel;
                    },
                    setVelocity : function(vel) {
                        console.log(vel);
                        this._vel.clone(vel);
                        return this;
                    },
                    behave : function(data) {
                        var bodies = this.getTargets();
                        for (var i = bodies.length - 1; i >= 0; i--) {
                            bodies[i].state.angular.pos = -0;
                            bodies[i].state.vel.x = this._vel.x;
                        };
                    }
                };
            });

            controller.vars.world.add(Physics.behavior('runner-constraints', {
                vel : {
                    x : 0.4,
                    y : 0
                }
            }).applyTo([controller.vars.runner]));

            controller.vars.world.add(Physics.behavior('constant-acceleration',{
                acc: {
                    x: 0,
                    y: 0.0007
                }
            }));
            controller.vars.world.add(Physics.behavior('body-impulse-response'));
            controller.vars.world.add(Physics.behavior('body-collision-detection'));
            controller.vars.world.add(Physics.behavior('sweep-prune'));
        },
        addBodies : function() {
            controller.vars.world.add(controller.vars.floor);
            controller.vars.world.add(controller.vars.runner);
        }
    },
    ui : {
        init : function() {

            controller.vars.background = [];

            controller.vars.background.push(platino.createSprite({
                image : 'images/track.png',
                width : 1024,
                height : 1024
            }));
            controller.vars.background.push(platino.createSprite({
                image : 'images/track.png',
                width : 1024,
                height : 1024,
                x : 1024
            }));

            controller.vars.floor = [];
            controller.vars.runner = Physics.body('rectangle', {
                width : 80,
                height : 80,
                x : 80,
                y : controller.vars.game.screen.height - 100,
                restitution : 0,
                cof : 0,
                options : {
                    isJumping : false,
                    isRunning : false,
                    change : false
                },
                treatment : 'dynamic'
            });

            controller.vars.runner.sprite = platino.createSpriteSheet({
                image : 'images/sprites/runner.png',
                width : 80,
                height : 80
            });

            var floorProperties = {
                width : 500,
                height : 40,
                x : 100,
                y : controller.vars.game.screen.height - 20,
                treatment : 'static',
                restitution : 0,
                cof : 0.3
            };

            for (var i = 0; i < 3; i++) {
                floorProperties.x = (floorProperties.width * i) + (floorProperties.width * 0.5);
                controller.vars.floor[i] = Physics.body('rectangle', floorProperties);
                controller.vars.floor[i].sprite = platino.createSprite({
                    width : floorProperties.width,
                    height : floorProperties.height,
                });

                controller.vars.floor[i].sprite.color(i === 0, i === 1, i === 2);
            };

            for (var i = 0,
                j = controller.vars.background.length; i < j; i++) {
                controller.actions.scene.add(controller.vars.background[i]);
            }
            for (var i = 0,
                j = controller.vars.floor.length; i < j; i++) {
                controller.actions.scene.add(controller.vars.floor[i].sprite);
            }
            controller.actions.scene.add(controller.vars.runner.sprite);

        }
    },
    resume : function(e) {
    },
    addHandlers : function() {
        controller.actions.scene.addEventListener('activated', function(e) {
            controller.ui.init();
            Physics(controller.physics.init);
            controller.vars.runner.sprite.animate(0, 12, 70, -1);
            controller.vars.game.startCurrentScene();
        });

        controller.actions.scene.addEventListener('touchstart', function() {
            controller.vars.runner.applyForce({
                x : 0,
                y : -0.04
            });
        });

    },
    actions : {
        scene : platino.createScene(),
        create : function(win, game) {
            controller.vars.window = win;
            controller.vars.game = game;
            controller.vars.halfScreen = game.screen.width * 0.5;
            console.log('half', controller.vars.halfScreen);
            return controller.actions.scene;
        },
        stepDelta : function(delta) {
            if (controller.vars.world && controller.vars.world.stepDelta) {
                controller.vars.world.stepDelta(delta);
            }

            cameraTransform1 = platino.createTransform();
            cameraTransform1.lookAt_eyeX = controller.vars.halfScreen - 80 + controller.vars.runner.state.pos.x;
            //controller.vars.game.camera.eyeX + (0.37 * delta);
            //controller.vars.runner.state.pos.x = parseInt(controller.vars.runner.state.pos.x + (0.37 * delta));
            cameraTransform1.duration = delta;
            controller.vars.game.moveCamera(cameraTransform1);

            for (var i = 0,
                j = controller.vars.floor.length; i < j; i++) {
                if (controller.vars.game.camera.eyeX - controller.vars.halfScreen > controller.vars.floor[i].state.pos.x + (controller.vars.floor[i].width * 0.5)) {
                    controller.vars.floor[i].state.pos.x = controller.vars.floor[i - 1 < 0 ? controller.vars.floor.length - 1 : i - 1].state.pos.x + controller.vars.floor[i].width;
                }

            };

            for (var i = 0,
                j = controller.vars.background.length; i < j; i++) {
                if (controller.vars.game.camera.eyeX - controller.vars.halfScreen > controller.vars.background[i].x + controller.vars.background[i].width) {
                    controller.vars.background[i].x = controller.vars.background[i - 1 < 0 ? controller.vars.background.length - 1 : i - 1].x + controller.vars.background[i].width;
                }

            };

        }
    },
    listeners : {
        activeScene : function() {

        }
    },
    models : {}
};

exports = _.extend(exports, controller.actions);

controller.init();

