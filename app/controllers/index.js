var game = Alloy.Globals.Platino.createGameView();
game.fps = 30;
game.color(0, 0, 0);
game.debug = false;
game.usePerspective = true;
game.enableOnDrawFrameEvent = true;
game.correctionHint = Alloy.Globals.Platino.OPENGL_FASTEST;

game.screen = {
    width : Alloy.Globals.size.width,
    height : Alloy.Globals.size.height
};

game.TARGET_SCREEN = {
    width : Alloy.Globals.size.width,
    height : Alloy.Globals.size.height
};

var updateScreenSize = function() {
    var screenScale = game.size.height / game.TARGET_SCREEN.height;
    game.screen = {
        width : game.size.width / screenScale,
        height : game.size.height / screenScale
    };
};

game.addEventListener('onload', function(e) {
    game.currentScene = require('mainScene');
    game.pushScene(new game.currentScene.create($.index, game));
    //var MainScene = require('mainScene-bk');
    //game.currentScene = new MainScene($.index, game);
    //game.pushScene(game.currentScene);
    game.start();
});

game.addEventListener('onsurfacechanged', function(e) {
    game.orientation = e.orientation;
    updateScreenSize();
});

game.addEventListener('enterframe', function(e) {
    if (game.currentScene && game.currentScene.stepDelta) {
        game.currentScene.stepDelta(e.delta);
    }
});

game.addEventListener('onfps', function(e) {
    Ti.API.info(e.fps.toFixed(2) + " fps");
});

$.index.add(game);
$.index.open();
